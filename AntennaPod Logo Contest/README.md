AntennaPod Logo Contest
-----------------------

The designs in this directory are provided with an unlimited and unrevokable license to the AntennaPod project, allowing its use and future adaptations/iterations.
